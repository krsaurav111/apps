package com.example.demo;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.boot.SpringApplication;

import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.kms.KmsClient;
import software.amazon.awssdk.services.kms.model.MessageType;
import software.amazon.awssdk.services.kms.model.SignRequest;
import software.amazon.awssdk.services.kms.model.SigningAlgorithmSpec;
import software.amazon.awssdk.services.kms.model.VerifyRequest;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import software.amazon.awssdk.core.SdkBytes;

import com.amazonaws.services.lambda.runtime.RequestHandler;

@SpringBootApplication
public class DemoLambda1Application{
	final static String key = "AKIASFUUPKGSTKGIHJ5A";
	final static String secretKey = "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";
	final static String keyArn = "arn:aws:kms:us-west-1:149561495973:key/01c69ef4-848b-44b8-a5d4-7bcb3081bb4d";
	public KmsClient kmsClient;
	
	public DemoLambda1Application() {
	    AwsBasicCredentials awsCreds = AwsBasicCredentials.create(key,
	      secretKey);
	    this.kmsClient = KmsClient.builder()
	       .credentialsProvider(StaticCredentialsProvider.create(awsCreds))
	       .region(Region.US_WEST_1).build();
	  }
	
	public static void main(String[] args){
		SpringApplication.run(DemoLambda1Application.class, args);
		String msg = "hello";
		MessageDigest md=null;
		try {
			md = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		md.update(msg.getBytes());
		byte[] digest = md.digest();
		SignRequest signRequest = SignRequest.builder()
                .keyId(keyArn)
                .messageType(MessageType.DIGEST)
                .signingAlgorithm("RSASSA_PSS_SHA_256")
                .message(SdkBytes.fromByteArray(digest))
                .build();
		System.out.println("hello");
		
	}

}
