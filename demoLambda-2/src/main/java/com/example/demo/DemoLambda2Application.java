package com.example.demo;

import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import org.springframework.boot.SpringApplication;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.core.SdkBytes;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.kms.KmsClient;
import software.amazon.awssdk.services.kms.model.MessageType;
import software.amazon.awssdk.services.kms.model.SignRequest;
import software.amazon.awssdk.services.kms.model.SigningAlgorithmSpec;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoLambda2Application {
	/*final static String key = "AKIASFUUPKGSTKGIHJ5A";
	final static String secretKey = "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";
	final static String keyArn = "arn:aws:kms:us-west-1:149561495973:key/01c69ef4-848b-44b8-a5d4-7bcb3081bb4d";
	public KmsClient kmsClient;
	
	public DemoLambda2Application() {
	    AwsBasicCredentials awsCreds = AwsBasicCredentials.create(key,
	      secretKey);
	    this.kmsClient = KmsClient.builder()
	       .credentialsProvider(StaticCredentialsProvider.create(awsCreds))
	       .region(Region.US_WEST_1).build();
	  }*/
	public static void main(String[] args) {
		SpringApplication.run(DemoLambda2Application.class, args);
		/*String msg = "hello";
		MessageDigest md=null;
		try {
			md = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		md.update(msg.getBytes());
		byte[] digest = md.digest();
		try {
            SignRequest signRequest = SignRequest.builder().signingAlgorithm(SigningAlgorithmSpec.ECDSA_SHA_256)
                    .keyId(keyArn)
                    .messageType(MessageType.DIGEST)
                    .message(SdkBytes.fromByteArray(digest))
                    .build();
        } finally {
            System.out.println("hello");
        }*/
		String base64EncodedKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAof98BTi+Y6343T4BoWSdU5KBUmoVtHA9/Q7dEYLtlS/NeWzsIpDxcZ4DhEx8vl2hhJAVFh0GE31lp1ojgfn6KjtGK+6zt/nB0tPMRHyeCmFFS/5jqZg3pGgOTjQPHfJhWSRQj++X/GyB8cUaoe4UFgBmp2CLlVfCfgrajX6Lbrd38JYVwXW9ev0bi3XHJKgL9YD/p12AQ21Krsr7Il2eU6BEKzO6NbCGWvKT3R1F5kTvtZWhNMNf6R6e9Q2+7F8x2Ljt8txWUCozg2R33vEjffsllRWXuw9AzWcBeCcm4AK+218VDAIZuQ9IxzVFsWNerOYE0rEgfoVoc+8590FfwQIDAQAB";
		byte[] pubKeyBytes = Base64.getMimeDecoder().decode(base64EncodedKey);
		X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(pubKeyBytes);
		PublicKey publicKey=null;
		try {
			publicKey = KeyFactory.getInstance("RSA").generatePublic(pubKeySpec);
		} catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Signature sig=null;
		try {
			sig = Signature.getInstance("SHA512withRSA");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			sig.initVerify(publicKey);
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		byte[] rawMessage = "hello world".getBytes(StandardCharsets.UTF_8);

		
		try {
			sig.update(rawMessage);
		} catch (SignatureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String signResult = "UmzOxIxvLkeZ+PAJvLjBH6vr5SjmTLpwbgZJ4szbpYUVZNHDLhz8sMlmVPM/8nm1Ek4M/fPZxSBqdcnnhFeon/OPmCnHsCaH76roTDhlkZt0JmCmuDapRp53uPpjWV/4WounQx7vK0n0gzmRj+SZ+rn30r4LajM0VOi5I2P65fFY72mOEK1PBZUDaMwN9sE68G7sqzDh+7d9OJ/DHvfgq+NcOyKe0AxYem7Y/EfRuLh8WCwz1QzgSaSzdnCfnRkV90tv4VW9kiz78oKZNwyvt6VSosYdcd56qk63finGgXk6CHZ1Ve4loBojA1lh9Jtl251WSaSgHK4HV2n9vYWMfw==";
		//byte[] signatureBytes = signResult.getSignature().array();
		byte[] signatureBytes = Base64.getDecoder().decode(signResult); 
		//byte[] finalSignature = signResult.getBytes(StandardCharsets.UTF_8);
		System.out.println(signatureBytes.length);
		boolean signatureValid = false;
		try {
			signatureValid = sig.verify(signatureBytes);
			System.out.println(signatureValid);
		} catch (SignatureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
