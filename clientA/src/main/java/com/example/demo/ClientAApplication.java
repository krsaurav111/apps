package com.example.demo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class ClientAApplication {
	private static String sendGET() throws IOException {
		URL obj = new URL("https://vmr3n5nr35.execute-api.ap-south-1.amazonaws.com/test/verifysignature");
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("POST");
		con.setDoOutput(true);
		con.setRequestProperty("User-Agent", "saurav");
		String jsonInputString = "{\"signature\": \"lX12ZIVpPMEX456GpphzS7jAZrsg18o2uti2GEuymOA/xeu9O2iaM45kBJj2A5poDp5pxau3S4rmHEEGii2BIWyi2HXVYt+Q3mt9fITEYOL6B3sZ3bPJc41eXoVTBOdilgL+Es3SKDkXp3fO4CgWvHtHt0bfS2CxtlG7zIYkBLR589SF53tIQ+bAYcg+YGiIE2T96lL9+NZPinV/7KPX2NlBQmyvLt9ij/xQB5hSryDzv5VY59phfOBmsw2+eEmy1Kcid2D76hsOmuivnrqJM4E0e8PgDWbHWlS0jMFo4HVAci/1hdOLugWesf6XpqH0bKq8n8aPm59ubkqCTMqrYQ==\", \"message\": \"create transaction\"}";
		try(OutputStream os = con.getOutputStream()) {
		    byte[] input = jsonInputString.getBytes("utf-8");
		    os.write(input, 0, input.length);			
		}
		int responseCode = con.getResponseCode();
		System.out.println("GET Response Code :: " + responseCode);
		if (responseCode == HttpURLConnection.HTTP_OK) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(
					con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			String res = response.toString();
			JSONObject jsonObject = null;
			try {
			     jsonObject = new JSONObject(res);
			     //System.out.println(jsonObject.getLong("status"));
			}catch (JSONException err){
			     err.printStackTrace();
			}
			long status = jsonObject.getLong("status");
			if(status == 200) {
				return "transaction successfull";
			}
			else {
				return "transaction failed";
			}
			
		}
		return "Response Code :: " + responseCode;
	}
	
	@GetMapping("/createTransaction")
	public String createTransaction() {
		String status= null;
		try {
			status = sendGET();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return status;
	}
	
	@GetMapping("/updateTransaction")
	public void updateTransaction() {
		System.out.println("success");
	}
		
	public static void main(String[] args) {
		SpringApplication.run(ClientAApplication.class, args);
		
	}

}
