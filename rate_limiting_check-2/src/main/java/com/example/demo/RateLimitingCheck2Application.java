package com.example.demo;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RestController
public class RateLimitingCheck2Application {
	
	@Bean
    public RouterFunction<ServerResponse> route(ChannelHandler channelHandler) {
        return RouterFunctions
                .route(GET("/api/{id}"), channelHandler::getRes);
    }

    @Component
    public class ChannelHandler {
        public Mono<ServerResponse> getRes(ServerRequest request) {
            Mono<String> name = Mono.just("Channel " + request.pathVariable("id"));
            return ok().body(name, String.class);
        }
    }
	public static void main(String[] args) {
		SpringApplication.run(RateLimitingCheck2Application.class, args);
	}

}
