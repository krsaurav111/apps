package com.example.demo;

import java.nio.charset.StandardCharsets;

import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.kms.KmsClient;
import software.amazon.awssdk.services.kms.model.MessageType;
import software.amazon.awssdk.services.kms.model.SignRequest;
import software.amazon.awssdk.services.kms.model.SignResponse;
import software.amazon.awssdk.services.kms.model.SigningAlgorithmSpec;
import software.amazon.awssdk.core.SdkBytes;
import java.util.Base64;
import org.springframework.web.bind.annotation.RequestHeader;


@SpringBootApplication
@RestController
public class KmsSignApplication {
	
	final static String key = "AKIASFUUPKGSZJSG5UVD";
	final static String secretKey = "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";
	//final static String keyArn = "arn:aws:kms:ap-south-1:149561495973:key/b48d9759-4745-4c6b-b211-8221e7a77aaa";
	
	@GetMapping("/sign")
	public String generateSignature(@RequestHeader String keyId, @RequestHeader String authMessage ) {
		KmsClient kmsClient;
		
		AwsBasicCredentials awsCreds = AwsBasicCredentials.create(key,
			      secretKey);
	    kmsClient = KmsClient.builder()
			       .credentialsProvider(StaticCredentialsProvider.create(awsCreds))
			       .region(Region.AP_SOUTH_1).build();
			    
		byte[] message = authMessage.getBytes(StandardCharsets.UTF_8);;
		SignRequest signReq =SignRequest.builder()
                .signingAlgorithm(SigningAlgorithmSpec.RSASSA_PKCS1_V1_5_SHA_512)
                .keyId(keyId)
                .messageType(MessageType.RAW)
                .message(SdkBytes.fromByteArray(message))
                .build();
		SignResponse signResult = kmsClient.sign(signReq);
		SdkBytes signatureSdkBytes = signResult.signature();
		String base64Signature = Base64.getEncoder().encodeToString(signatureSdkBytes.asByteArray());
		return base64Signature;
	}
	
	public static void main(String[] args) {
		SpringApplication.run(KmsSignApplication.class, args);
	}

}
