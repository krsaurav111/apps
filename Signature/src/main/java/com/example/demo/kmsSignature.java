package com.example.demo;

import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import org.json.JSONObject;


import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.secretsmanager.AWSSecretsManager;
import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder;
import com.amazonaws.services.secretsmanager.model.DecryptionFailureException;
import com.amazonaws.services.secretsmanager.model.GetSecretValueRequest;
import com.amazonaws.services.secretsmanager.model.GetSecretValueResult;
import com.amazonaws.services.secretsmanager.model.InternalServiceErrorException;
import com.amazonaws.services.secretsmanager.model.InvalidParameterException;
import com.amazonaws.services.secretsmanager.model.InvalidRequestException;
import com.amazonaws.services.secretsmanager.model.ResourceNotFoundException;

public class kmsSignature {
	//final static String key = "AKIASFUUPKGSZJSG5UVD";
	//final static String secretKey = "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";
	private static String key;
	private static String secretKey;
	final static String region = "ap-south-1";
	private KeyPair pair;
	
	public kmsSignature(String userKey, String userSecretKey) throws Exception {
		this.key=userKey;
		this.secretKey=userSecretKey;
		
		KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance("RSA");
	      
	      //Initializing the key pair generator
	    keyPairGen.initialize(2048);
	    this.pair = keyPairGen.generateKeyPair();
    }
	
	public String getPrivateKey() {
		PrivateKey privKey = pair.getPrivate();
	    byte[] privateKeyBytes = privKey.getEncoded();
	    String base64PrivKey = Base64.getEncoder().encodeToString(privateKeyBytes);
	    return base64PrivKey;
	}
	
	public String getPublicKey() {
		PublicKey pubKey = pair.getPublic();
	    byte[] publicKeyBytes = pubKey.getEncoded();
	    String base64PubKey = Base64.getEncoder().encodeToString(publicKeyBytes);
	    return base64PubKey;
	}
	
	public static String getValue(String secretName) {
        // Create a Secrets Manager client
		BasicAWSCredentials awsCreds = new BasicAWSCredentials(key, secretKey);
		AWSSecretsManager client = AWSSecretsManagerClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(awsCreds)).withRegion (region).build();
        
        // In this sample we only handle the specific exceptions for the 'GetSecretValue' API.
        // See https://docs.aws.amazon.com/secretsmanager/latest/apireference/API_GetSecretValue.html
        // We rethrow the exception by default.
        
        String secret = "", decodedBinarySecret;
        GetSecretValueRequest getSecretValueRequest = new GetSecretValueRequest()
                        .withSecretId(secretName);
        GetSecretValueResult getSecretValueResult = null;

        try {
            getSecretValueResult = client.getSecretValue(getSecretValueRequest);
        } catch (DecryptionFailureException e) {
            // Secrets Manager can't decrypt the protected secret text using the provided KMS key.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e;
        } catch (InternalServiceErrorException e) {
            // An error occurred on the server side.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e;
        } catch (InvalidParameterException e) {
            // You provided an invalid value for a parameter.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e;
        } catch (InvalidRequestException e) {
            // You provided a parameter value that is not valid for the current state of the resource.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e;
        } catch (ResourceNotFoundException e) {
            // We can't find the resource that you asked for.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e;
        }

        // Decrypts secret using the associated KMS CMK.
        // Depending on whether the secret is a string or binary, one of these fields will be populated.
        if (getSecretValueResult.getSecretString() != null) {
            secret = getSecretValueResult.getSecretString();
        }
        else {
            decodedBinarySecret = new String(Base64.getDecoder().decode(getSecretValueResult.getSecretBinary()).array());
        }
        JSONObject jsonObject = new JSONObject(secret); 
        return jsonObject.getString("pubKey");
    }
	
	
	public String generate (String privKey, String authMessage ) throws Exception{
		byte[] privKeyBytes = Base64.getMimeDecoder().decode(privKey);
		//X509EncodedKeySpec privKeySpec = new X509EncodedKeySpec(privKeyBytes);
		PKCS8EncodedKeySpec privKeySpec = new PKCS8EncodedKeySpec(privKeyBytes);
		PrivateKey privateKey = KeyFactory.getInstance("RSA").generatePrivate(privKeySpec);
		
		Signature sign = Signature.getInstance("SHA512withRSA");

	      //Initializing the signature
	      sign.initSign(privateKey);
	      byte[] bytes = authMessage.getBytes();
	      
	      //Adding data to the signature
	      sign.update(bytes);
	      
	      //Calculating the signature
	      byte[] signature = sign.sign();
	      String base64Signature = Base64.getEncoder().encodeToString(signature);
	      System.out.println(base64Signature);
	      return base64Signature;
	}
	
	public boolean verify(String signResult, String message, String publcKey) {
		//String base64EncodedKey = getValue(secretARN);
		byte[] pubKeyBytes = Base64.getMimeDecoder().decode(publcKey);
		X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(pubKeyBytes);
		PublicKey publicKey=null;
		
		try {
			publicKey = KeyFactory.getInstance("RSA").generatePublic(pubKeySpec);
		} catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Signature sig=null;
		try {
			sig = Signature.getInstance("SHA512withRSA");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			sig.initVerify(publicKey);
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//String message = mapInput.get("message");
		byte[] rawMessage = message.getBytes(StandardCharsets.UTF_8);
		
		try {
			sig.update(rawMessage);
		} catch (SignatureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//String signResult = mapInput.get("signature");
		//byte[] signatureBytes = signResult.getSignature().array();
		byte[] signatureBytes = Base64.getDecoder().decode(signResult); 
		//byte[] finalSignature = signResult.getBytes(StandardCharsets.UTF_8);
		boolean signatureValid = false;
		try {
			signatureValid = sig.verify(signatureBytes);
		} catch (SignatureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return signatureValid;
	}

}

