package com.example.demo;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Base64;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SignatureApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(SignatureApplication.class, args);
		kmsSignature kmsSign = new kmsSignature("AKIASFUUPKGSZJSG5UVD","laejX8W5fODwXodvvWxaVpy2gPrarz6BQ+Al+NlY");
		System.out.println(kmsSign.verify("YZrzWCkw/+zi5KTUpxe+XURW5uE7eRy9cd+8bHG3PANCakUkLLWLJSwp1blz68kXM42i/KAuYMUhmaOL2hgvCdSWi+ivOVcQQcw0HNJrwS6d8YMptv54dC8OpCMkmXwTHJgUyXqfEsBHlaoO5H0r6d46Sw3h43hJF0q/CY9SDgoAvw1tCsJmGbJopJShm+Z4BvdD/LUFFIR4n4fSMQacs30DH8ZqHXoIgTF/mGIOs6LLkMYik/ulWcCIQn2vlVb8fY+qMTWdr3YzHjDScREBGaAezjs5QDvxhA+EvpPUz7vjN7CEA2p7NLWZwA3+yOR5sM5EHW/UCPHpvuvmaODQ4w==", 
				"hello world", "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtQPLkFqKCN2SXw77dLB3rtlvFFn3CdohDGCby6a/g5pqE1Mgs/3IWR2xQ7gea9r4e/LNbmm7jHS08zJHmH6FTX2XeqPkEyxYnQ882ZVBNhZUu+6okTEOYPWggpz0eNymXMW1ASTXDMZ0wIuyTX8HY5Kic1reB3y0LcVCxusv46cu99avjWPDCFA2M6AHY3gx4ZhXNnSNx9cGXNGavOPIStnNuYFRQdZOOgBmWE7AderkIzVvMAPBcnjfB5kHqNKLrjxfBOZrhL3AOLhHHR3ZK9xXlxr+irixy3DBeg6oVA5Ua/APpBMGWW6tX/tRxSQD7ISusitlw1Haa+ey8oIuywIDAQAB"));
		//System.out.println(kmsSign.generate("arn:aws:kms:ap-south-1:149561495973:key/b48d9759-4745-4c6b-b211-8221e7a77aaa", "hello world"));
		/*KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance("RSA");
	      
	      //Initializing the key pair generator
	     keyPairGen.initialize(2048);
		      
	      //Generate the pair of keys
	     KeyPair pair = keyPairGen.generateKeyPair();
	      
	      //Getting the privatekey from the key pair
	     PrivateKey privKey = pair.getPrivate();
	     byte[] privateKeyBytes = privKey.getEncoded();
	     String base64PrivKey = Base64.getEncoder().encodeToString(privateKeyBytes);
	     
	   //Getting the publickey from the key pair
	     PublicKey pubKey = pair.getPublic();
	     byte[] publicKeyBytes = pubKey.getEncoded();
	     String base64PubKey = Base64.getEncoder().encodeToString(publicKeyBytes);*/
		 String base64PrivKey = kmsSign.getPrivateKey();
		 String base64PubKey = kmsSign.getPublicKey();
		 
	     kmsSign.generate(base64PrivKey, "hello world");
	     System.out.println(base64PubKey);
	     System.out.println(base64PrivKey);
	     
	     
	}

}
