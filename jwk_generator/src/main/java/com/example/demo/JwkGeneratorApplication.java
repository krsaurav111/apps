package com.example.demo;

import java.util.*;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.gen.RSAKeyGenerator;

import java.io.File;
import java.io.FileOutputStream;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Map;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

@SpringBootApplication
@RestController
public class JwkGeneratorApplication {
	
	

	public static void main(String[] args) throws Exception {
		SpringApplication.run(JwkGeneratorApplication.class, args);
		

	}
	
	@GetMapping("/jwt")
	public String getToken() throws Exception {
		Map<String, Object> keys = generateRSAKeys();
		
		String b64PubKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzaB7qf0e/hgI97AdGjB/maix6pHiXTyPc2BOcSbGNIMXF7hcDk0zRLuAffC0Ww4NQzWFoVs4AxxDK5JmSC4Eho6oKePA/OZGOvJZnRwV1Kg2TA9UAeFU7Q08rUWxiTt2inmgTKb5yzKdwG9rMXivrUs5bNoJ3Tn9t+765s/RVDL3vMOCGWcvWXuIB+ZKrv8jPCb5zZuTtMdyevsCXIKuCEta5L1mhvwPboQOJ2oJxwFl7S9hQAHjfACK7QBiyZqpNMnc0ngZcyjuGeKY3+RiafnY26u6hlOmDB5a97QNW1bRbsfeLEA2AR1lgJui3GRK92uzS3wfdIPE5cgSNGHp7QIDAQAB";
		String b64PrvKey = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDNoHup/R7+GAj3sB0aMH+ZqLHqkeJdPI9zYE5xJsY0gxcXuFwOTTNEu4B98LRbDg1DNYWhWzgDHEMrkmZILgSGjqgp48D85kY68lmdHBXUqDZMD1QB4VTtDTytRbGJO3aKeaBMpvnLMp3Ab2sxeK+tSzls2gndOf237vrmz9FUMve8w4IZZy9Ze4gH5kqu/yM8JvnNm5O0x3J6+wJcgq4IS1rkvWaG/A9uhA4nagnHAWXtL2FAAeN8AIrtAGLJmqk0ydzSeBlzKO4Z4pjf5GJp+djbq7qGU6YMHlr3tA1bVtFux94sQDYBHWWAm6LcZEr3a7NLfB90g8TlyBI0YentAgMBAAECggEABq8fIKEevx8O45Dw++dJro1XmIq8yKUYUKV3x0vTCOVIpze8nlEPcpOEaL5Io+Zqs0hBG26jUnk6YbyKNfHGVI1nP/1W9Pbon4U949KEOdEXUBT2I8gsV4xPmvOiAsD7vc1C/bA2fHlyZqKjNwzM68gp31+duc2RxscuQWR4pyZLZZMYflpTqBv4gIEPSpuOnBoF3xxBGLMLSt3irTt+GkNr8SNdnUtq8hg6JQZaLIgrkpGiAxImyPbdR0w00Ga2bv67xHXvUV/pLvSnLlR+Cw22E7cZAguwsQlIhziBqJoRo8uaoIq0UcuBe1l8G2jjrWYSjlHT9xWVIzs0DcmKAQKBgQDrK7mado2V3J10C/P0eXgdHQhnGAjqniZxU9p1GLIryZYTaIXpz90uR3ARI2MWyD8PeHgpdEWS722lnJZztSbhJp+921mstnFF6+Kb4zwZsmxzsSJ6BUpKMD1rA0utXETrPRY+89578SZt3J1x67Ys23XRqgtff0Kr30tj4zzlgQKBgQDf1uC3/s0FTptuVZC0OKFGVTMVzDCJplWKE3ppuISuk5rG/1L+2+2zlksygC6fAs2zf4GXxC90L3HmdM6KvvLmacDXZsBmK7nfg3WyhjXCGikUFLl5Fh9XXYtE5MgVhMVITvIaGHUCtGB7hf6oM1WVKX1ROHJboKEqjWVVeSEybQKBgQCoz6ZX3qob3VWZK8tevqMeIu0ZNAOShxwJVawHSkb7b6UvIWSfRfr7cYRciQ5kGDJMJKn8993Jb91BeHn3bR4PmO4qFv70H7KIAp+UTzWhLJb1JT4eYRXG04IDA0ablxRR2HPTlLMsPRrc2fSbBhyrbo3Qh0PKtUtinNM9TK02AQKBgH9+9Da5UmwQbyX0ZNzSWMIn61YiHQ96h8SC/bnUGPcbKhDpA4wZFxawp5pXOrblsObp3JXmcCUclSLwK2kKBG7Lk8TdSP+pLlEqntKV8W7i2b0PZOmceTOZG6edLH5dRsW9+dZ1Uci9lyskkn2Z58hzfmbR4QlhwArtyOdbfFYtAoGAO9dSYUQHjtOcmE/fJfGmn702fwbkbDGXNE17moKCe+MuCn6XSGDYwHNdXMYsRHc1Ubg330E2XW934UI/T77zDPqkBaQodP4Wgy/5aE6cfN2JTMo2iYsMt1q++itMg0i0hfr+X8oN16MpwOuvN41K9qUsH+IBw9v826ei5LAT6nY=";
		
		KeyFactory kfPub = KeyFactory.getInstance("RSA");
        X509EncodedKeySpec keyPubSpecX509 = new X509EncodedKeySpec(Base64.getDecoder().decode(b64PubKey));
        RSAPublicKey pubKey = (RSAPublicKey) kfPub.generatePublic(keyPubSpecX509);
        System.out.println(pubKey);
        
        byte [] encoded = Base64.getDecoder().decode(b64PrvKey);
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encoded);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        RSAPrivateKey privKey = (RSAPrivateKey) kf.generatePrivate(keySpec);
        System.out.println(privKey);
        //RSAPrivateKey prvKey = fact.generatePrivate(spec);
        /*KeyFactory kfPrv = KeyFactory.getInstance("RSA");
        X509EncodedKeySpec keyPrvSpecX509 = new X509EncodedKeySpec(Base64.getDecoder().decode(b64PrvKey));
        RSAPrivateKey prvKey = (RSAPrivateKey) kfPrv.generatePrivate(keyPrvSpecX509);*/
        
		//RSAPrivateKey privateKey;
        String token = null;
        try {
            //privateKey = (RSAPrivateKey) keys.get("private");
            
            Algorithm algorithm = Algorithm.RSA256(null, privKey);
            token = JWT.create()
                    .withIssuer("example")
                    .sign(algorithm);
        } catch (JWTCreationException x) {
            throw x;
        }
        RSAPublicKey publicKey;
        try {
            //publicKey = (RSAPublicKey) keys.get("public");
            Algorithm algorithm = Algorithm.RSA256(pubKey, null);
            JWTVerifier verifier = JWT.require(algorithm)
                    .withIssuer("example")
                    .build();
            DecodedJWT jwt = verifier.verify(token);

        } catch (JWTVerificationException x) {
            throw x;
        }
        
        RSAPublicKey rsa = (RSAPublicKey) pubKey;
        
        /*byte[] encodedPublicKey = publicKey.getEncoded();
        String b64PublicKey = Base64.getEncoder().encodeToString(encodedPublicKey);
        System.out.println(b64PublicKey);
        
        byte[] encodedPrivateKey = privateKey.getEncoded();
        String b64PrivateKey = Base64.getEncoder().encodeToString(encodedPrivateKey);
        System.out.println(b64PrivateKey);
        
        KeyFactory kf = KeyFactory.getInstance("RSA");
        X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(Base64.getDecoder().decode(b64PublicKey));
        RSAPublicKey pubKey = (RSAPublicKey) kf.generatePublic(keySpecX509);
        //System.out.println(pubKey);*/
        Map<String, Object> values = new HashMap<>();

        values.put("kty", rsa.getAlgorithm());
        values.put("kid", "someuniqueid");
        values.put("n", Base64.getUrlEncoder().encodeToString(rsa.getModulus().toByteArray()));
        values.put("e", Base64.getUrlEncoder().encodeToString(rsa.getPublicExponent().toByteArray()));
        values.put("alg", "RSA256");
        values.put("use", "sig");
        System.out.println(values);
        
        String pcKey = "{\"keys\":[{\"use\": \"sig\",\"alg\": \"RS256\",\"kid\": \"test\",\"kty\": \"RSA\",\"n\": \"" + values.get("n") + "\",\"e\": \"AQAB\"}]}";
        
        System.out.println(pcKey);
        //System.out.println(publicKey);
        
        return token;
	}
	
	private static Map<String, Object> generateRSAKeys() throws Exception {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(2048);

        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        return Map.of("private", keyPair.getPrivate(), "public", keyPair.getPublic());
    }

}
