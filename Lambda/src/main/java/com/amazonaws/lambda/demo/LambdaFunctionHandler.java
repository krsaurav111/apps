package com.amazonaws.lambda.demo;

import com.amazonaws.services.lambda.runtime.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.HashMap;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import software.amazon.awssdk.services.ssm.SsmClient;
import software.amazon.awssdk.services.ssm.model.GetParameterRequest;
import software.amazon.awssdk.services.ssm.model.GetParameterResponse;
import software.amazon.awssdk.services.ssm.model.SsmException;
import software.amazon.awssdk.regions.Region;


class Content {
	private String status;

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}
}

public class LambdaFunctionHandler implements RequestHandler<Object, Object> {
	
	private static int sendGET(String url) throws IOException  {
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		con.setDoOutput(true);
		int responseCode = con.getResponseCode();
		return responseCode;
	}
	
	public String getParaValue(SsmClient ssmClient, String paraName) {
		String publcKey="";
        try {
            GetParameterRequest parameterRequest = GetParameterRequest.builder()
                .name(paraName)
                .build();

            GetParameterResponse parameterResponse = ssmClient.getParameter(parameterRequest);
            publcKey=parameterResponse.parameter().value();

        } catch (SsmException e) {
        System.err.println(e.getMessage());
        System.exit(1);
        }
        return publcKey;
   }
    @Override
    public Object handleRequest(Object input, Context context) {
    	HashMap<String, String> mapInput = (HashMap<String, String>) input;
    	
    	String paraName = "pubKey";
        Region region = Region.AP_SOUTH_1;
        SsmClient ssmClient = SsmClient.builder()
                .region(region)
                .build();
        String publcKey=getParaValue(ssmClient, paraName);
        ssmClient.close();
        
		String base64EncodedKey = publcKey;
		byte[] pubKeyBytes = Base64.getMimeDecoder().decode(base64EncodedKey);
		X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(pubKeyBytes);
		PublicKey publicKey=null;
		
		try {
			publicKey = KeyFactory.getInstance("RSA").generatePublic(pubKeySpec);
		} catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Signature sig=null;
		try {
			sig = Signature.getInstance("SHA512withRSA");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			sig.initVerify(publicKey);
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String message = mapInput.get("message");
		byte[] rawMessage = message.getBytes(StandardCharsets.UTF_8);
		
		try {
			sig.update(rawMessage);
		} catch (SignatureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String signResult = mapInput.get("signature");
		//byte[] signatureBytes = signResult.getSignature().array();
		byte[] signatureBytes = Base64.getDecoder().decode(signResult); 
		//byte[] finalSignature = signResult.getBytes(StandardCharsets.UTF_8);
		System.out.println(signatureBytes.length);
		boolean signatureValid = false;
		try {
			signatureValid = sig.verify(signatureBytes);
			System.out.println(signatureValid);
		} catch (SignatureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    	Content content = new Content();
    	if(signatureValid) {
    		if(message.equals("create transaction")) {
    			int st=0;
				try {
					st = sendGET("https://4e64-2402-3a80-b0c-3392-bf5c-710a-c188-9b0b.ngrok.io/createTransaction");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		if(st == 200) {
	    			content.setStatus("200");
	    		}
	    		else {
	    			content.setStatus("503");
	    		}
    		}
    		else {
    			int st=0;
				try {
					st = sendGET("https://1431-2402-3a80-b0c-3392-bf5c-710a-c188-9b0b.ngrok.io/updateTransaction");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		if(st == 200) {
	    			content.setStatus("200");
	    		}
	    		else {
	    			content.setStatus("503");
	    		}
    			content.setStatus("200");
    		}
    	}
    	else {
    		content.setStatus("503");
    	}
    	
    	return content;
    }

}
