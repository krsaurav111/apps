package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class RateLimitingCheck1Application {
	
	@GetMapping("/1")
	public String home1() {
		return "home1";
	}
	
	@GetMapping("/2")
	public String home2() {
		return "home2";
	}
	
	@GetMapping("/3")
	public String home3() {
		return "home3";
	}
	
	public static void main(String[] args) {
		SpringApplication.run(RateLimitingCheck1Application.class, args);
	}

}
