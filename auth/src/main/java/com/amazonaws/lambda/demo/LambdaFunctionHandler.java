package com.amazonaws.lambda.demo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.amazonaws.Request;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import software.amazon.awssdk.services.ssm.SsmClient;
import software.amazon.awssdk.services.ssm.model.GetParameterRequest;
import software.amazon.awssdk.services.ssm.model.GetParameterResponse;
import software.amazon.awssdk.services.ssm.model.SsmException;
import software.amazon.awssdk.regions.Region;

public class LambdaFunctionHandler implements RequestHandler<Map<String,Object>, Map<String,Object>> {
	
	private Map<String, Object> generatePolicy(String principalId, String effect, String resource) {
		Map<String, Object> authResponse = new HashMap<>();
		System.out.println(principalId);
		authResponse.put("principalId", principalId);
		Map<String, Object> policyDocument = new HashMap<>();
		policyDocument.put("Version", "2012-10-17"); // default version
		Map<String, String> statementOne = new HashMap<>();
		statementOne.put("Action", "execute-api:Invoke"); // default action
		statementOne.put("Effect", effect);
		statementOne.put("Resource", resource);
		policyDocument.put("Statement", new Object[] {statementOne});
		authResponse.put("policyDocument", policyDocument);
		if ("Allow".equals(effect)) {
			Map<String, Object> context = new HashMap<>();
			context.put("key", "value");
			context.put("numKey", Long.valueOf(1L));
			context.put("boolKey", Boolean.TRUE);
			authResponse.put("context", context);
		}
		return authResponse;
	}
	
	public String getParaValue(SsmClient ssmClient, String paraName) {
		String publcKey="";
        try {
            GetParameterRequest parameterRequest = GetParameterRequest.builder()
                .name(paraName)
                .build();

            GetParameterResponse parameterResponse = ssmClient.getParameter(parameterRequest);
            publcKey=parameterResponse.parameter().value();

        } catch (SsmException e) {
        System.err.println(e.getMessage());
        System.exit(1);
        }
        return publcKey;
   }

    @Override
    public Map<String, Object> handleRequest(Map<String,Object> event, Context context) {
    	HashMap<String, Object> mapInput = (HashMap<String, Object>) event.get("headers");
    	String signResult = mapInput.get("signature").toString();
    	String message = mapInput.get("message").toString();
    	String paraName = "pubKey";
        Region region = Region.AP_SOUTH_1;
        SsmClient ssmClient = SsmClient.builder()
                .region(region)
                .build();
        String publcKey=getParaValue(ssmClient, paraName);
        ssmClient.close();
        
		String base64EncodedKey = publcKey;
		byte[] pubKeyBytes = Base64.getMimeDecoder().decode(base64EncodedKey);
		X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(pubKeyBytes);
		PublicKey publicKey=null;
		
		try {
			publicKey = KeyFactory.getInstance("RSA").generatePublic(pubKeySpec);
		} catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Signature sig=null;
		try {
			sig = Signature.getInstance("SHA512withRSA");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			sig.initVerify(publicKey);
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//String message = mapInput.get("message");
		byte[] rawMessage = message.getBytes(StandardCharsets.UTF_8);
		
		try {
			sig.update(rawMessage);
		} catch (SignatureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//String signResult = mapInput.get("signature");
		//byte[] signatureBytes = signResult.getSignature().array();
		byte[] signatureBytes = Base64.getDecoder().decode(signResult); 
		//byte[] finalSignature = signResult.getBytes(StandardCharsets.UTF_8);
		System.out.println(signatureBytes.length);
		boolean signatureValid = false;
		try {
			signatureValid = sig.verify(signatureBytes);
			System.out.println(signatureValid);
		} catch (SignatureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String effect = "Deny";
		if(signatureValid) {
			effect = "Allow";
		}
        // TODO: implement your handler
        return generatePolicy("149561495973", effect, "arn:aws:execute-api:ap-south-1:149561495973:5f8f5b8cfh/*/GET/custom");
    }

}
