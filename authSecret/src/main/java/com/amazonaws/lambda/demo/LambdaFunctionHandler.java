package com.amazonaws.lambda.demo;

import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.amazonaws.services.lambda.runtime.Context;

import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.secretsmanager.AWSSecretsManager;
import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder;
import com.amazonaws.services.secretsmanager.model.DecryptionFailureException;
import com.amazonaws.services.secretsmanager.model.GetSecretValueRequest;
import com.amazonaws.services.secretsmanager.model.GetSecretValueResult;
import com.amazonaws.services.secretsmanager.model.InternalServiceErrorException;
import com.amazonaws.services.secretsmanager.model.InvalidParameterException;
import com.amazonaws.services.secretsmanager.model.InvalidRequestException;
import com.amazonaws.services.secretsmanager.model.ResourceNotFoundException;

public class LambdaFunctionHandler implements RequestHandler<Map<String,Object>, Map<String, Object>> {
	
	private String pub="null";
	
	private Map<String, Object> generatePolicy(String principalId, String effect, String resource) {
		Map<String, Object> authResponse = new HashMap<>();
		authResponse.put("principalId", principalId);
		//authResponse.put("paraTime", paraTime);
		Map<String, Object> policyDocument = new HashMap<>();
		policyDocument.put("Version", "2012-10-17"); // default version
		Map<String, String> statementOne = new HashMap<>();
		statementOne.put("Action", "execute-api:Invoke"); // default action
		statementOne.put("Effect", effect);
		statementOne.put("Resource", resource);
		policyDocument.put("Statement", new Object[] {statementOne});
		authResponse.put("policyDocument", policyDocument);
		if ("Allow".equals(effect)) {
			Map<String, Object> context = new HashMap<>();
			context.put("key", "value");
			context.put("numKey", Long.valueOf(1L));
			context.put("boolKey", Boolean.TRUE);
			authResponse.put("context", context);
		}
		return authResponse;
	}
	public static String getValue(String secretName, String region) {

        // Create a Secrets Manager client
        AWSSecretsManager client  = AWSSecretsManagerClientBuilder.standard()
                                        .withRegion(region)
                                        .build();
        
        // In this sample we only handle the specific exceptions for the 'GetSecretValue' API.
        // See https://docs.aws.amazon.com/secretsmanager/latest/apireference/API_GetSecretValue.html
        // We rethrow the exception by default.
        
        String secret = "", decodedBinarySecret;
        GetSecretValueRequest getSecretValueRequest = new GetSecretValueRequest()
                        .withSecretId(secretName);
        GetSecretValueResult getSecretValueResult = null;

        try {
            getSecretValueResult = client.getSecretValue(getSecretValueRequest);
        } catch (DecryptionFailureException e) {
            // Secrets Manager can't decrypt the protected secret text using the provided KMS key.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e;
        } catch (InternalServiceErrorException e) {
            // An error occurred on the server side.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e;
        } catch (InvalidParameterException e) {
            // You provided an invalid value for a parameter.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e;
        } catch (InvalidRequestException e) {
            // You provided a parameter value that is not valid for the current state of the resource.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e;
        } catch (ResourceNotFoundException e) {
            // We can't find the resource that you asked for.
            // Deal with the exception here, and/or rethrow at your discretion.
            throw e;
        }

        // Decrypts secret using the associated KMS CMK.
        // Depending on whether the secret is a string or binary, one of these fields will be populated.
        if (getSecretValueResult.getSecretString() != null) {
            secret = getSecretValueResult.getSecretString();
        }
        else {
            decodedBinarySecret = new String(Base64.getDecoder().decode(getSecretValueResult.getSecretBinary()).array());
        }
        JSONObject jsonObject = new JSONObject(secret); 
        return jsonObject.getString("pubKey");
    }
	
	public LambdaFunctionHandler() {
		String secretName = "arn:aws:secretsmanager:ap-south-1:233815244996:secret:pubKey-tE5Oxo";
        String region = "ap-south-1";
        //return publcKey;
        pub = getValue(secretName, region);
    }
	
    @Override
    public Map<String, Object> handleRequest(Map<String,Object> event, Context context) {
        // TODO: implement your handler
    	System.out.println("event called");
    	HashMap<String, Object> mapInput = (HashMap<String, Object>) event.get("headers");
    	String signResult = mapInput.get("signature").toString();
    	String message = mapInput.get("message").toString();
    	
    	String publcKey = pub;
		String base64EncodedKey = publcKey;
		byte[] pubKeyBytes = Base64.getMimeDecoder().decode(base64EncodedKey);
		X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(pubKeyBytes);
		PublicKey publicKey=null;
		
		try {
			publicKey = KeyFactory.getInstance("RSA").generatePublic(pubKeySpec);
		} catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Signature sig=null;
		try {
			sig = Signature.getInstance("SHA512withRSA");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			sig.initVerify(publicKey);
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//String message = mapInput.get("message");
		byte[] rawMessage = message.getBytes(StandardCharsets.UTF_8);
		
		try {
			sig.update(rawMessage);
		} catch (SignatureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//String signResult = mapInput.get("signature");
		//byte[] signatureBytes = signResult.getSignature().array();
		byte[] signatureBytes = Base64.getDecoder().decode(signResult); 
		//byte[] finalSignature = signResult.getBytes(StandardCharsets.UTF_8);
		boolean signatureValid = false;
		try {
			signatureValid = sig.verify(signatureBytes);
		} catch (SignatureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String effect = "Deny";
		if(signatureValid) {
			effect = "Allow";
		}
        return generatePolicy("233815244996", effect, "arn:aws:execute-api:ap-south-1:233815244996:2u2nrsaic1/*/GET/verifysignature");
    }

}
