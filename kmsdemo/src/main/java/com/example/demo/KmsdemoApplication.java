package com.example.demo;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.FileChannel;
import java.nio.file.Paths;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.boot.SpringApplication;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.core.SdkBytes;
import software.amazon.awssdk.regions.Region;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.services.kms.KmsClient;
import software.amazon.awssdk.services.kms.model.DataKeySpec;
import software.amazon.awssdk.services.kms.model.DecryptRequest;
import software.amazon.awssdk.services.kms.model.DecryptResponse;
import software.amazon.awssdk.services.kms.model.EncryptRequest;
import software.amazon.awssdk.services.kms.model.EncryptResponse;
import software.amazon.awssdk.services.kms.model.GenerateDataKeyRequest;
import software.amazon.awssdk.services.kms.model.GenerateDataKeyResponse;

@SpringBootApplication
public class KmsdemoApplication {
	
	final static String key = "AKIASFUUPKGS2LQS54QM";
	final static String secretKey = "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";
	final static String keyArn = "arn:aws:kms:us-west-1:149561495973:key/5c103c21-bc85-4d8d-bff8-94688f9bd0b9";
	
	public KmsClient kmsClient;
	
	public KmsdemoApplication() {
	    AwsBasicCredentials awsCreds = AwsBasicCredentials.create(key,
	      secretKey);
	    this.kmsClient = KmsClient.builder()
	       .credentialsProvider(StaticCredentialsProvider.create(awsCreds))
	       .region(Region.US_WEST_1).build();
	  }
	
	public SdkBytes encrypt(SdkBytes jsonString) {
		EncryptRequest encryptRequest = EncryptRequest.builder().keyId(keyArn).plaintext(jsonString).build();
		EncryptResponse encryptResponse = this.kmsClient.encrypt(encryptRequest);
		return encryptResponse.ciphertextBlob();
	}
	
	public static void writeToFile(SdkBytes bytesToWrite, String path) throws IOException {
		FileChannel fc;
		FileOutputStream outputStream = new FileOutputStream(path);
		fc = outputStream.getChannel();
		fc.write(bytesToWrite.asByteBuffer());
		outputStream.close(); 
		fc.close();
	}
	
	public SdkBytes decrypt(SdkBytes encryptedJsonString) {
		DecryptRequest decryptRequest = DecryptRequest.builder().ciphertextBlob(encryptedJsonString).build();
		DecryptResponse decryptResponse = this.kmsClient.decrypt(decryptRequest);
		return decryptResponse.plaintext();
	}
	
	public static SdkBytes readFromFile(String path) throws IOException {
	    InputStream in2 = new FileInputStream(path);
	    return SdkBytes.fromInputStream(in2);
	  }
	
	public void encryptUsingDataKey(SdkBytes jsonString) {
		try {
			GenerateDataKeyRequest generateDataKeyRequest = GenerateDataKeyRequest.builder().keyId(keyArn).keySpec(DataKeySpec.AES_128).build();
			GenerateDataKeyResponse generateDataKeyResponse = this.kmsClient.generateDataKey(generateDataKeyRequest);
			
			SecretKeySpec key = new SecretKeySpec(generateDataKeyResponse.plaintext().asByteArray(),"AES");
			Cipher cipher;
			cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] encodedSecret = cipher.doFinal(jsonString.asByteArray());
			byte[] encryptedDataKey = key.getEncoded();
			String path = Paths.get(".").toAbsolutePath().normalize().toString() + "/observation_datakey_encrypt.json";
			
			KmsdemoApplication.writeToFile(SdkBytes.fromByteArray(encodedSecret), path);
			
			path = Paths.get(".").toAbsolutePath().normalize().toString() + "/data_key_encrypt.json";
			KmsdemoApplication.writeToFile(SdkBytes.fromByteArray(encryptedDataKey), path);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		SpringApplication.run(KmsdemoApplication.class, args);

		try {
			KmsdemoApplication kmsExample = new KmsdemoApplication();
			InputStream in = kmsExample.getClass().getClassLoader()
				      .getResourceAsStream("observation.json");
			SdkBytes inputBytes = SdkBytes.fromInputStream(in);
			
			/*SdkBytes outputBytes = kmsExample.encrypt(inputBytes);
			
			String path = Paths.get(".").toAbsolutePath().normalize().toString() + "/observation_encrypt.json";
			System.out.println(path);
			KmsdemoApplication.writeToFile(outputBytes, path);
			SdkBytes output2Bytes = kmsExample.decrypt(KmsdemoApplication
				      .readFromFile(path));

				    System.out.println(output2Bytes.asUtf8String());*/
				    kmsExample.encryptUsingDataKey(inputBytes);
		    } 
		    catch (Exception e) {
		      e.printStackTrace();
		    }
	}

}
