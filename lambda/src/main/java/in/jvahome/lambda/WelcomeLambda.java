package in.jvahome.lambda;

import com.amazonaws.services.lambda.runtime.Context;


import com.amazonaws.services.lambda.runtime.RequestHandler;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.HashMap;

/**
 * Hello world!
 *
 */

class Content {
	private String status;

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}


}

public class WelcomeLambda implements RequestHandler<Object, Object>
{
    public Object handleRequest(Object input, Context context) {
    	HashMap<String, String> mapInput = (HashMap<String, String>) input;
		String base64EncodedKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAof98BTi+Y6343T4BoWSdU5KBUmoVtHA9/Q7dEYLtlS/NeWzsIpDxcZ4DhEx8vl2hhJAVFh0GE31lp1ojgfn6KjtGK+6zt/nB0tPMRHyeCmFFS/5jqZg3pGgOTjQPHfJhWSRQj++X/GyB8cUaoe4UFgBmp2CLlVfCfgrajX6Lbrd38JYVwXW9ev0bi3XHJKgL9YD/p12AQ21Krsr7Il2eU6BEKzO6NbCGWvKT3R1F5kTvtZWhNMNf6R6e9Q2+7F8x2Ljt8txWUCozg2R33vEjffsllRWXuw9AzWcBeCcm4AK+218VDAIZuQ9IxzVFsWNerOYE0rEgfoVoc+8590FfwQIDAQAB";
		byte[] pubKeyBytes = Base64.getMimeDecoder().decode(base64EncodedKey);
		X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(pubKeyBytes);
		PublicKey publicKey=null;
		try {
			publicKey = KeyFactory.getInstance("RSA").generatePublic(pubKeySpec);
		} catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Signature sig=null;
		try {
			sig = Signature.getInstance("SHA512withRSA");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			sig.initVerify(publicKey);
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String message = mapInput.get("message");
		byte[] rawMessage = message.getBytes(StandardCharsets.UTF_8);

		
		try {
			sig.update(rawMessage);
		} catch (SignatureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String signResult = mapInput.get("signature");
		//byte[] signatureBytes = signResult.getSignature().array();
		byte[] signatureBytes = Base64.getDecoder().decode(signResult); 
		//byte[] finalSignature = signResult.getBytes(StandardCharsets.UTF_8);
		System.out.println(signatureBytes.length);
		boolean signatureValid = false;
		try {
			signatureValid = sig.verify(signatureBytes);
			System.out.println(signatureValid);
		} catch (SignatureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	System.out.println("Hello");
    	Content content = new Content();
    	if(signatureValid) {
    		content.setStatus("200");
    	}
    	else {
    		content.setStatus("503");
    	}
    	return content;
    }
}
