package com.example.demo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@SpringBootApplication
@RestController
public class ClientBApplication {
	
	private static String sendGET() throws IOException {
		URL obj = new URL("https://vmr3n5nr35.execute-api.ap-south-1.amazonaws.com/test/verifysignature");
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("POST");
		con.setDoOutput(true);
		con.setRequestProperty("User-Agent", "saurav");
		String jsonInputString = "{\"signature\": \"KjaZEdWrgUdmr61MIhzboZ392wYROwymP4sU81q2dmRecGU1ZVBrZOcJ3SZ+V23acjXx0xwEE8W4OLj6Kx2TSnTUUBFMsp3ZQDpS5Htl00AknEIjJbxT+9Itx8a2e9QMrOveEds+qZRHyWFp3c8XtI6hzQxQGjMWr1qK+RY+m+doqc/RlYJvfVIH9w8/KQUTt/dU6QMV+mNWKSNw1OjLvWb0rg4bw9n0RPWaZt7v/i1Gl+xnG3OUjDiysbs0c9dEg9+xKZKI8Ffq6vxgGb3j1H7buOt2KX1b1aAWh7kbNnGKGapsoxh1ltCPAIGx7uGTLYwSjj7hKWPlZEEXC1FWNg==\", \"message\": \"update transaction\"}";
		try(OutputStream os = con.getOutputStream()) {
		    byte[] input = jsonInputString.getBytes("utf-8");
		    os.write(input, 0, input.length);			
		}
		int responseCode = con.getResponseCode();
		System.out.println("GET Response Code :: " + responseCode);
		if (responseCode == HttpURLConnection.HTTP_OK) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(
					con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			String res = response.toString();
			JSONObject jsonObject = null;
			try {
			     jsonObject = new JSONObject(res);
			     //System.out.println(jsonObject.getLong("status"));
			}catch (JSONException err){
			     err.printStackTrace();
			}
			long status = jsonObject.getLong("status");
			if(status == 200) {
				return "transaction updated";
			}
			else {
				return "transaction update failed";
			}
			
		}
		return "Response Code :: " + responseCode;
	}
	
	@GetMapping("/createTransaction")
	public void createTransaction() {
		System.out.println("success");
	}
	
	@GetMapping("/updateTransaction")
	public String updateTransaction() {
		String res = "";
		try {
			res = sendGET();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return res;
	}
	public static void main(String[] args) {
		SpringApplication.run(ClientBApplication.class, args);
	}

}
